package lab2;

import java.util.ArrayList;

public class Chapter {
	String capitol;
    ArrayList<SubChapter> subcapitole;
    
    public Chapter(String c) {
        subcapitole = new ArrayList<SubChapter>();
        capitol = c;
    }
    
    public int createSubChapter(String subCapitol) {
        subcapitole.add(new SubChapter(subCapitol));
        return subcapitole.size() - 1;
    }
    
    public SubChapter getSubChapter(int indexSubChapter) {
        return subcapitole.get(indexSubChapter);
    }
    
    public String toString() {
        String s = "\t";
        for (int i = 0; i < subcapitole.size(); i++)
            s += subcapitole.get(i) + "\n\t";
        return capitol + "\n" + s;
    }
}