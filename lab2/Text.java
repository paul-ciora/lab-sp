package lab2;

public class Text extends Content {
	String text;
    
    public Text(String t) {
        text = t;
    }
    
    public String toString() {
    	return "Text: " + text;
    }
}