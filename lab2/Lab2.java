package lab2;

public class Lab2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book myBook = new Book("Disco Titanic");
		Author gheo = new Author("Radiu Pavel Gheo");
		myBook.addAuthor(gheo);
		int indexChapterOne = myBook.createChapter("Capitolul 1");
		int indexSubChapterOneOne = myBook.createSubChapter(indexChapterOne, "Subcapitolul 1");
		Text textOneOne = myBook.createText(indexChapterOne, indexSubChapterOneOne, "My first paragraph");
		System.out.println(myBook);
	}

}