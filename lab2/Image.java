package lab2;

public class Image extends Content {
	String imagine;
    
    public Image(String i) {
        imagine = i;
    }
    
    public String toString() {
    	return "Imagine: " + imagine;
    }
}