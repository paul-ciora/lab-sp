package lab2;

import java.util.ArrayList;

public class SubChapter {
	String subCapitol;
    ArrayList<Content> continut;
    
    public SubChapter(String s) {
        continut = new ArrayList<Content>();
        subCapitol = s;
    }
    
    public void createContent(Content c) {
        continut.add(c);
    }
    
    public String toString() {
    	String c = "\t\t";
    	for (int i = 0; i < continut.size(); i++)
    		c += continut.get(i) + "\n\t\t";
    	return subCapitol + "\n" + c;
    }
}