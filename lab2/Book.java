package lab2;

import java.util.ArrayList;

public class Book {
	String titlu;
    ArrayList<Author> autori;
    ArrayList<Chapter> capitole;
    
    public Book(String t) {
        autori = null;
        autori = new ArrayList<Author>();
        capitole = new ArrayList<Chapter>();
        titlu = t;
    }
    
    public void addAuthor(Author autor) {
        autori.add(autor);
    }
    
    public int createChapter(String capitol) {
        capitole.add(new Chapter(capitol));
        return capitole.size() - 1;
    }
    
    public int createSubChapter(int indexChapter, String subCapitol) {
        return capitole.get(indexChapter).createSubChapter(subCapitol);
    }
    
    public Text createText(int indexChapter, int indexSubChapter, String text) {
        Text t = new Text(text);
        capitole.get(indexChapter).getSubChapter(indexSubChapter).createContent(t);
        return t;
    }
    
    public Table createTable(int indexChapter, int indexSubChapter, String tabel) {
        Table t = new Table(tabel);
        capitole.get(indexChapter).getSubChapter(indexSubChapter).createContent(t);
        return t;
    }
    
    public Image createImage(int indexChapter, int indexSubChapter, String imagine) {
        Image img = new Image(imagine);
        capitole.get(indexChapter).getSubChapter(indexSubChapter).createContent(img);
        return img;
    }
    
    public String toString() {
        String a = "";
        for (int i = 0; i < autori.size(); i++)
            a += autori.get(i) + ", ";
        String continut = "";
        for (int i = 0; i < capitole.size(); i++)
            continut += capitole.get(i) + "\n\n";
        return "Titlu: " + titlu + "\nAutori: " + a + "\n\n" + continut;
    }
}